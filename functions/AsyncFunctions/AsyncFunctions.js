var axios = require("axios");


var qs = {
    name: "",
    desc: "",
    idList: '5e221451ef9aa88aa1bf4b6e',
    idLabels: ["5ea07ed5c9cf6d6aed044877"],
    keepFromSource: 'all',
    key: '2894988be3697ee1371add8160c30535',
    token: '9f383e2633398f9b4df3cea8800447b58561eceabeca55abe37d9a0df626d19e'
}

module.exports.uploadCardToTrello = function(data) {
    console.log('UPLOADING!');
    const description = "Contacto\n"
            + "Correo electrónico: " + data.email + "\n"
            + "Telefono: " + data.phone + "\n\n"
            + "Información personal\n"
            + data.bio

    qs.name = data.name
    qs.desc = description
    return axios.post('https://api.trello.com/1/cards', qs).then(response => {
        //console.log("!!Response data!! ", response.data)
        return response.data.id
    }).catch(error => {
        console.log("!!error!!", error)
        return error
    })
}
