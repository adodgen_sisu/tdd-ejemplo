//const test = require('firebase-functions-test')();
//let jest = require('jest')
var axios = require("axios");
const asyncFunctions = require('../AsyncFunctions/AsyncFunctions')

let post = jest.fn(() => Promise.resolve({data:{id:"12345678"}}))
axios.post = post

describe("test suite - async functions", () => {
  test('Test 1 - upload card to Trello mock', async () => {
    let id = await asyncFunctions.uploadCardToTrello({name: "mock name", email: "mock email", phone: "mock phone", bio: "mock bio" }) 
    expect(id.length).toEqual(8)
  });



})
