import React from 'react';
import { render } from '@testing-library/react';
import { shallow, mount } from 'enzyme';

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, getAllUsers, signInWithEmailAndPassword, uploadTrelloCard, signOut } from './AsyncFunctions'

import firebase from '../firebase';

import { firestore } from './firebaseMocks/Firestore'
firebase.firestore = firestore;
import { storage, fileSnapshot } from './firebaseMocks/Storage'
firebase.storage = storage;
import { auth } from './firebaseMocks/Auth'
firebase.auth = auth
import { functions } from './firebaseMocks/Functions'
firebase.functions = functions;

//ERRORS
import {authNotFoundError, authInvalidPassError} from './firebaseMocks/errors/AuthErrors'
import {firestoreAddError} from './firebaseMocks/errors/FirestoreErrors'
import {functionsInternalError} from './firebaseMocks/errors/FunctionsInternalError'





describe('Test suite - Testing the mocked async functions', () => {

  test('Test 1 - uploadUser(data) - Mock upload document to firestore users', async () => {

    let userDoc = await uploadUser({ name: "This is a new document", email: "email@email.com", phone: "12345678910", bio: "This is a new document bio", })
    expect(userDoc.id.length).toEqual(20)
    expect(firestore().collection).toHaveBeenCalledWith("uploadedUsers");
  });

  test('Test 2 - uploadCVToStorage(cv, date) - Mock upload file to storage and return fileSnapshot', async () => {
    let currDate = Date.now()
    let fileObject = { name: "MyFile" }

    let fileSnapshot = await uploadCVToStorage(fileObject, currDate)

    expect(fileSnapshot.ref).not.toEqual(undefined)
    expect(fileSnapshot).toEqual(fileSnapshot)
    expect(storage().ref().child).toHaveBeenCalledWith(`cv/${"MyFile"}_${currDate}`)
  });

  test('Test 3 - getFileURL(fileSnapshot) - Mock get download url from fileSnapshot', async () => {
    let URL = await getFileURL(fileSnapshot)

    expect(URL).toEqual("https://exampleDownloadURL/1unbcuain324nfb/f23n098n2f3")
  });

  test('Test 4 - setUserCV(docID, downloadURL) - Mock upload file to storage and recieve download url', async () => {
    let currDate = Date.now()
    let fileObject = { name: "MyFile" }

    let fileSnapshot = await uploadCVToStorage(fileObject, currDate)
    let URL = await getFileURL(fileSnapshot)

    expect(URL).toEqual("https://exampleDownloadURL/1unbcuain324nfb/f23n098n2f3")
    expect(storage().ref().child).toHaveBeenCalledWith(`cv/${"MyFile"}_${currDate}`)
  });

  test('Test 5 - setUserCV(docID, downloadURL) - Mock set user document', async () => {
    await setUserCV("2hd749sm193nd85jgns1", "https://exampleDownloadURL/1unbcuain324nfb/f23n098n2f3")

    expect(firestore().collection).toHaveBeenCalledWith("uploadedUsers");
    expect(firestore().collection().doc).toHaveBeenCalledWith("2hd749sm193nd85jgns1");
    expect(firestore().collection().doc().set).toHaveBeenCalled();
    expect(firestore().collection().doc().set).toHaveBeenCalledWith({cv: "https://exampleDownloadURL/1unbcuain324nfb/f23n098n2f3"}, { merge: true });
  });


  test('Test 6 - uploadTrelloCard(name, email, phone, bio) - Should upload trello card and get id response', async () => {
    let response = await uploadTrelloCard()
    expect(response.data.id).toEqual("12345678")
  });


  test('Test 7 - getAllUsers() - Get collection of users', async () => {
    let users = await getAllUsers()

    expect(users.empty).toEqual(false)
    expect(firestore().collection).toHaveBeenCalledWith("uploadedUsers");
  });

  test('Test 8 - signInWithEmailAndPassword(email, password) - Test sign in with email password', async () => {
    let returnedUser = await signInWithEmailAndPassword("username", "password")

    expect(returnedUser.user.uid).not.toEqual(undefined)
    expect(returnedUser.user.uid.length).toEqual(20)

  });


  test('Test 9 - signOut() - Test auth sign out user', async () => {
    await signOut()

    expect(auth().signOut).toHaveBeenCalled()
  });

});



describe('Test suite 2 - Testing the mocked errors', () => {

  test('Test 1 - Test auth/auth/wrong-password', async () => {
    firebase.auth = authInvalidPassError

    try{
      let currentUser = await signInWithEmailAndPassword("email", "password")
      console.log(currentUser)
    }catch(error){
      console.log(error)
      expect(error.message).toEqual("The provided value for the password user property is invalid. It must be a string with at least six characters")
      expect(error.code).toEqual('auth/wrong-password')

    }
    
  });/**/

  test('Test 2 - Test auth/user-not-found error', async () => {
    firebase.auth = authNotFoundError

    try{
      let currentUser = await signInWithEmailAndPassword("email", "password")
      console.log(currentUser)
    }catch(error){
      console.log(error)
      expect(error.message).toEqual("There is no existing user record corresponding to the provided identifier.")
      expect(error.code).toEqual('auth/user-not-found')

    }
    
  });/**/

  test('Test 3 - Firestore add error', async () => {
    firebase.firestore = firestoreAddError
    try{
      let userDoc = await uploadUser({ name: "This is a new document", email: "email@email.com", phone: "12345678910", bio: "This is a new document bio", })
    }catch(error){
      expect(error.message).toEqual("ERROR ADDING DOC")
      expect(error.code).toEqual(771)

    }
    
  });

  test('Test 4 - Firestore functions internal error', async () => {
    firebase.functions = functionsInternalError
    try{
      let response = await uploadTrelloCard("name", "email", "phone", "bio")
    }catch(error){
      expect(error.message).toEqual("Internal error")
      expect(error.code).toEqual(500)

    }
    
  });

})