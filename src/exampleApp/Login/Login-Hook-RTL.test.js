import React from 'react';

import { shallow, mount } from 'enzyme';
import { render, fireEvent, act, waitForElement, waitForElementToBeRemoved } from '@testing-library/react'

import LoginHook from './LoginHook'

import firebase from '../../firebase';
import { auth } from '../firebaseMocks/Auth'
import {authNotFoundError, authInvalidPassError} from '../firebaseMocks/errors/AuthErrors'

firebase.auth = auth;

const flushPromises = () => new Promise(setImmediate);


describe('Test suite A - User input', () => {

  it('Test 1 - email input component', () => {
    const { getByTestId, getByText } = render(<LoginHook />)
    expect(getByTestId("email")).toBeTruthy()
  });
  it('Test 2 - password input component', () => {
    const { getByTestId, getByText } = render(<LoginHook />)
    expect(getByTestId("password")).toBeTruthy()
  });
  it('Test 3 - submit button component', () => {
    const { getByTestId, getByText } = render(<LoginHook />)
    expect(getByTestId("myButton")).toBeTruthy()
  });

  it('Test 4 - test email input initialization', () => {
    const { getByTestId, getByText } = render(<LoginHook />)
    const inputEmail = getByTestId("email")

    expect(inputEmail.value).toBe("")

  });

  it('Test 5 - test password input initialization', () => {
    const { getByTestId, getByText } = render(<LoginHook />)
    const inputPassword = getByTestId("password")

    expect(inputPassword.value).toBe("")

  });

  it('Test 5 - user input works', () => {
    const { getByTestId, getByText } = render(<LoginHook />)
    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")

    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@sisu.mx" } })
    fireEvent.change(inputPassword, { target: { name:"password", value: "123456" } })

    expect(inputEmail.value).toBe("adodgen@sisu.mx")
    expect(inputPassword.value).toBe("123456")
  });

  });
  


describe('Test suite B - User input errors working correctly', () => {

  it('Test 1 - Calls submit only (email) error', async () => {
    const { getByTestId, getByText } = render(<LoginHook />)

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputPassword, { target: { name:"password", value: "123456" } })
    expect(inputPassword.value).toBe("123456")

    fireEvent.click(myButton)

    expect(getByTestId("emailRequiredLabel")).toBeTruthy()
  });

  it('Test 2 - Calls submit only (password) error', async () => {
    const { getByTestId, getByText } = render(<LoginHook />)

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@sisu.mx" } })
    expect(inputEmail.value).toBe("adodgen@sisu.mx")

    fireEvent.click(myButton)

    expect(getByTestId("passwordRequiredLabel")).toBeTruthy()
  });

  it('Test 3 - Calls submit only (email format) error', async () => {
    const { getByTestId, getByText } = render(<LoginHook />)

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@" } })
    fireEvent.change(inputPassword, { target: { name:"password", value: "123456" } })
    expect(inputEmail.value).toBe("adodgen@")
    expect(inputPassword.value).toBe("123456")

    fireEvent.click(myButton)

    expect(getByTestId("emailFormatLabel")).toBeTruthy()
  });

  it('Test 4 - Calls submit only (password length) error', async () => {
    const { getByTestId, getByText } = render(<LoginHook />)

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@sisu.mx" } })
    fireEvent.change(inputPassword, { target: { name:"password", value: "12345" } })
    expect(inputEmail.value).toBe("adodgen@sisu.mx")
    expect(inputPassword.value).toBe("12345")

    fireEvent.click(myButton)

    //You can also use toBeVisible() instead of toBeTruthy()
    expect(getByTestId("passwordLengthLabel")).toBeVisible()
  });


});



describe('Test suite C - Testing the submit form options', () => {
  let history = {push: jest.fn(() => {})}

  it('Calls submit form with user not found error', async () => {
    const { getByTestId, getByText } = render(<LoginHook history={history} />)
    firebase.auth = authNotFoundError

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@sisu.mx" } })
    fireEvent.change(inputPassword, { target: { name:"password", value: "123456" } })
    expect(inputEmail.value).toBe("adodgen@sisu.mx")
    expect(inputPassword.value).toBe("123456")

    await act(async () => {
      fireEvent.click(myButton)
    })

    expect(firebase.auth().signInWithEmailAndPassword).toHaveBeenCalled()
    expect(getByTestId("userMessage").innerHTML).toEqual("This email was not found...")

  })

  it('Calls submit form with invalid password error', async () => {
    const { getByTestId, getByText } = render(<LoginHook history={history} />)
    firebase.auth = authInvalidPassError

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@sisu.mx" } })
    fireEvent.change(inputPassword, { target: { name:"password", value: "123456" } })
    expect(inputEmail.value).toBe("adodgen@sisu.mx")
    expect(inputPassword.value).toBe("123456")

    await act(async () => {
      fireEvent.click(myButton)
    })

    expect(firebase.auth().signInWithEmailAndPassword).toHaveBeenCalled()
    expect(getByTestId("userMessage").innerHTML).toEqual("Wrong password detected...")
  
  })

  it('Calls submit form with page change', async () => {
    const { getByTestId, getByText } = render(<LoginHook history={history} />)
    firebase.auth = auth

    const inputEmail = getByTestId("email")
    const inputPassword = getByTestId("password")
    const myButton = getByTestId("myButton")


    expect(inputEmail.value).toBe("")
    expect(inputPassword.value).toBe("")

    fireEvent.change(inputEmail, { target: { name:"email", value: "adodgen@sisu.mx" } })
    fireEvent.change(inputPassword, { target: { name:"password", value: "123456" } })
    expect(inputEmail.value).toBe("adodgen@sisu.mx")
    expect(inputPassword.value).toBe("123456")

    await act(async () => {
      fireEvent.click(myButton)
    })

    expect(getByTestId("userMessage").innerHTML).toEqual("LOGGED IN! 28dj4mao60148cmtb25k")
    expect(history.push).toHaveBeenCalledWith("/users-table")

  })
});

