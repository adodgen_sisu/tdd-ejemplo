import React, { useState, useEffect, useGlobal } from 'react';
import firebase from '../../firebase'
import { signInWithEmailAndPassword, signOut } from '../AsyncFunctions'



const LoginHook = (props) => {
    const [state, setState] = useState({ email: "", password: "", userMessage: "", errors: [] })

    useEffect(() => {

    }, [])

    useEffect(() => {

    }, [state])

    const handleFormInputChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.value });
    }

    const checkForErrors = () => {
        let errors = []
        const required = ["email", "password"]
        required.forEach(field => {
            if (state[field] === "" || state[field] === undefined)
                errors.push(field)
        })

        if (!errors.includes("password") && state.password.length < 6)
            errors.push("passwordLength")

        let re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9_.-]+\.[a-zA-Z_0-9-]{2,}$/
        if (!errors.includes("email") && !re.test(state.email))
            errors.push("emailFormat")

        setState({ ...state, errors: errors })

        return errors.length > 0
    }

    const login = async () => {
        if (!checkForErrors()) {
            try {
                let returnedUser = await signInWithEmailAndPassword(state.email, state.password)

                setState({ ...state, userMessage: "LOGGED IN! " + returnedUser.user.uid })
                props.history.push("/users-table")

            }
            catch (error) {
                console.log(JSON.stringify(error), " - auth error")
                if (error.code === 'auth/user-not-found')
                    setState({ ...state, userMessage: "This email was not found..." })
                else if (error.code === 'auth/wrong-password')
                    setState({ ...state, userMessage: "Wrong password detected..." })
            }
        }
    }



    return (
        <div className="Login">
            <span className="Loginr_title">Login - (Hooks)</span>
            <div className="Login_inputs">


                <input className="email" name="email" data-testid="email" id="email"
                    type="text"
                    placeholder="email"
                    value={state.email}
                    onChange={handleFormInputChange}
                />
                <div >{state.errors.includes("email") && <label style={{ color: 'red' }} data-testid="emailRequiredLabel">This field is required</label>}</div>
                <div >{!state.errors.includes("email") && state.errors.includes("emailFormat") && <label style={{ color: 'red' }} data-testid="emailFormatLabel">Your email must be formatted properly</label>}</div>

                <input className="password" name="password" data-testid="password" id="password"
                    type="text"
                    placeholder="password"
                    value={state.password}
                    onChange={handleFormInputChange}
                />
                <div >{state.errors.includes("password") && <label style={{ color: 'red' }} data-testid="passwordRequiredLabel">This field is required</label>}</div>
                <div >{!state.errors.includes("password") && state.errors.includes("passwordLength") && <label style={{ color: 'red' }} data-testid="passwordLengthLabel">Your password must be minimum 6 characters</label>}</div>

                <button data-testid="myButton" type="submit" style={{ width: 200, height: 22, backgroundColor: 'gray' }} className="myButton" id="myButton" onClick={login}>Submit to Firestore</button>

            </div>

            <span data-testid="userMessage" id="userMessage">{state.userMessage}</span><div />

        </div>
    );
};

export default LoginHook