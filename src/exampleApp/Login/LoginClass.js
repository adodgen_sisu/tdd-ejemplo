import React, { Component } from 'react';
import firebase from 'firebase'
import { signInWithEmailAndPassword } from '../AsyncFunctions'



class Login extends Component {

    INITIAL_STATE = {
        email: "",
        password: "",
        userMessage: "",
        errors: []
    };

    constructor(props) {
        super(props);
        this.state = { ...this.INITIAL_STATE };
    }

    componentDidMount() {

    }

    handleFormInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    checkForErrors = () => {
        let errors = []
        const required = ["email", "password"]
        required.forEach(field => {
            if (this.state[field] === "" || this.state[field] === undefined)
                errors.push(field)
        })

        if (!errors.includes("password") && this.state.password.length < 6)
            errors.push("passwordLength")

        let re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9_.-]+\.[a-zA-Z_0-9-]{2,}$/
        if (!errors.includes("email") && !re.test(this.state.email))
            errors.push("emailFormat")

        this.setState({ errors: errors })

        return errors.length > 0
    }

    login = async () => {
        if (!this.checkForErrors()) {

            signInWithEmailAndPassword(this.state.email, this.state.password).then((returnedUser) => {
                //window.alert(JSON.stringify(returnedUser))
                this.props.history.push("/users-table")
                this.setState({ userMessage: "LOGGED IN! " + returnedUser.user.uid })
            }).catch(error => {
                console.log(JSON.stringify(error), " - auth error")
                if (error.code === 'auth/user-not-found')
                    this.setState({ userMessage: "This email was not found..." })
                else if (error.code === 'auth/wrong-password')
                    this.setState({ userMessage: "Wrong password detected..." })
            })
        }
    }

    render() {
        return (
            <div className="App">
                {<div className="myStatus" data-testid="userMessage">{this.state.userMessage}</div>}

                <input className="email" name="email" data-testid="email"
                    type="text"
                    placeholder="email"
                    value={this.state.email}
                    onChange={this.handleFormInputChange}
                />
                <div >{this.state.errors.includes("email") && <label style={{ color: 'red' }} data-testid="emailRequiredLabel">This field is required</label>}</div>
                <div >{!this.state.errors.includes("email") && this.state.errors.includes("emailFormat") && <label style={{ color: 'red' }} data-testid="emailFormatLabel">Your email must be formatted properly</label>}</div>

                <input className="password" name="password" data-testid="password"
                    type="text"
                    placeholder="password"
                    value={this.state.password}
                    onChange={this.handleFormInputChange}
                />
                <div >{this.state.errors.includes("password") && <label style={{ color: 'red' }} data-testid="passwordRequiredLabel">This field is required</label>}</div>
                <div >{!this.state.errors.includes("password") && this.state.errors.includes("passwordLength") && <label style={{ color: 'red' }} data-testid="passwordLengthLabel">Your password must be minimum 6 characters</label>}</div>
                <div />

                {<button type="submit" style={{ width: 200, height: 22, backgroundColor: 'gray' }} className="myButton" data-testid="myButton" onClick={this.login}>Submit to Firestore</button>}

            </div>
        )

    }
}


export default Login;