import React from 'react';

import { shallow, mount } from 'enzyme';
import { render, fireEvent, cleanup, wait, waitForElement } from "@testing-library/react";

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, getAllUsers, signInWithEmailAndPassword, uploadCardToTrello } from '../AsyncFunctions'

import Login from './LoginClass'

import firebase from '../../firebase';
import { auth } from '../firebaseMocks/Auth'
import {authNotFoundError, authInvalidPassError} from '../firebaseMocks/errors/AuthErrors'

firebase.auth = auth;


const flushPromises = () => new Promise(setImmediate);

describe('Test suite A - User input', () => {
  let wrapper = mount(<Login />);

  it('Test 1 - Test initialization of state variables', () => {
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().password).toEqual("");
    expect(wrapper.state().userMessage).toEqual("");
  });

  it('Test 2 - email input', () => {
    expect(wrapper.find(".email").exists()).toEqual(true)
  });
  it('Test 3 - password input', () => {
    expect(wrapper.find(".password").exists()).toEqual(true)
  });
  it('Test 4 - submit button', () => {
    expect(wrapper.find(".myButton").exists()).toEqual(true)
  });

  it('Test 5 - user input works', () => {
    wrapper = mount(<Login />);
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().password).toEqual("");
    
    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    wrapper.find('.password').simulate('change', { target: { name: 'password', value: '123456' } });

    expect(wrapper.state().email).toEqual("adodgen@sisu.mx");
    expect(wrapper.state().password).toEqual("123456");
  });

  
});



describe('Test suite B - User input errors working correctly', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = mount(<Login />);
  });
  it('Test 1 - Calls submit only (email) error', async () => {
    expect(wrapper.state().email).toEqual("");

    wrapper.find('.password').simulate('change', { target: { name: 'password', value: '123456' } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("email")).toEqual(true);
  });

  it('Test 2 - Calls submit only (password) error', async () => {
    expect(wrapper.state().password).toEqual("");

    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("password")).toEqual(true);
  });

  it('Test 3 - Calls submit only (email format) error', async () => {
    expect(wrapper.state().email).toEqual("");

    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgensisu.mx' } });
    wrapper.find('.password').simulate('change', { target: { name: 'password', value: '123456' } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("emailFormat")).toEqual(true);
  });

  it('Test 4 - Calls submit only (password length) error', async () => {
    expect(wrapper.state().password).toEqual("");

    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    wrapper.find('.password').simulate('change', { target: { name: 'password', value: '1234' } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("passwordLength")).toEqual(true);
  });


});



describe('Test suite C - Testing the submit form options', () => {
  let wrapper = null
  let history = {push: jest.fn(() => {})}
  beforeEach(() => {
    wrapper = mount(<Login history={history}/>);
  });


  it('Calls submit form with user not found error', async () => {
    firebase.auth = authNotFoundError
    expect(wrapper.state().userMessage).toEqual("");
  
    wrapper.find('.email').simulate('change', { target: { name: "email", value: 'mk@mkyl.com' } });
    wrapper.find('.password').simulate('change', { target: { name: "password", value: '1234506789' } });
  
    expect(wrapper.state().email).toEqual("mk@mkyl.com");
    expect(wrapper.state().password).toEqual("1234506789");
  
    wrapper.find('.myButton').simulate('click')
  
    await Promise.resolve();
    wrapper.update();

    expect(wrapper.state().userMessage).toEqual("This email was not found...");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("This email was not found...");
  
  })
  it('Calls submit form with invalid password error', async () => {
    firebase.auth = authInvalidPassError
    expect(wrapper.state().userMessage).toEqual("");
  
    wrapper.find('.email').simulate('change', { target: { name: "email", value: 'mk@mkyl.com' } });
    wrapper.find('.password').simulate('change', { target: { name: "password", value: '1234506789aaa' } });
  
    expect(wrapper.state().email).toEqual("mk@mkyl.com");
    expect(wrapper.state().password).toEqual("1234506789aaa");
  
    wrapper.find('.myButton').simulate('click')
  
    await Promise.resolve();
    wrapper.update();

    expect(wrapper.state().userMessage).toEqual("Wrong password detected...");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("Wrong password detected...");
  
  })

  it('Calls submit form with page change', async () => {
    firebase.auth = auth
    expect(wrapper.state().userMessage).toEqual("");
  
    wrapper.find('.email').simulate('change', { target: { name: "email", value: 'mk@mkyl.com' } });
    wrapper.find('.password').simulate('change', { target: { name: "password", value: '1234506789aaa' } });
  
    expect(wrapper.state().email).toEqual("mk@mkyl.com");
    expect(wrapper.state().password).toEqual("1234506789aaa");
  
    wrapper.find('.myButton').simulate('click')
  
    await Promise.resolve();
    wrapper.update();

    expect(history.push).toHaveBeenCalledWith('/users-table')
  })
});

