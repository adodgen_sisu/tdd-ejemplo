import firebase from "../firebase";


export function uploadUser(data) {
  return firebase.firestore().collection('uploadedUsers').add({
    name: data.name,
    email: data.email,
    phone: data.phone,
    bio: data.bio,
  })
}//Firestore

export function uploadCVToStorage(cv = { name: "testname" }, date) {
  let cvStoragePath = `cv/${cv.name}_${date}`
  const cvRef = firebase.storage().ref().child(cvStoragePath)

  return cvRef.put(cv)
}//Storage

export function getFileURL(fileSnapshot) {
  return fileSnapshot.ref.getDownloadURL()
}//Storage

export function setUserCV(docID, downloadURL) {
  return firebase.firestore().collection('uploadedUsers').doc(docID).set({
    cv: downloadURL,
  }, { merge: true })
}//Firestore

export function uploadTrelloCard(name, email, phone, bio) {
  
  let uploadCardToTrello = firebase.functions().httpsCallable('uploadToTrello');
  return uploadCardToTrello({name: name, email: email, phone: phone, bio: bio})
}//Functions

export function signInWithEmailAndPassword(email, password) {
  return firebase.auth().signInWithEmailAndPassword(email, password)
}//Auth


export function getAllUsers() {
  return firebase.firestore().collection('uploadedUsers').get()
}//Firestore

export function signOut() {
  return firebase.auth().signOut()
}//Auth