import React from 'react';

import { shallow, mount } from 'enzyme';
import { render, fireEvent, act, wait, waitFor, waitForElement, waitForElementToBeRemoved, toBeTruthy, getByTestId } from '@testing-library/react'

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, getAllUsers, signInWithEmailAndPassword, uploadCardToTrello } from '../AsyncFunctions'


import firebase from '../../firebase';


import { firestore } from '../firebaseMocks/Firestore'
import { firestoreAddError } from '../firebaseMocks/errors/FirestoreErrors'
firebase.firestore = firestore;
import { storage, fileSnapshot } from '../firebaseMocks/Storage'
firebase.storage = storage;
firebase.fileSnapshot = fileSnapshot;
import { auth } from '../firebaseMocks/Auth'
firebase.auth = auth;
import { functions } from '../firebaseMocks/Functions'
firebase.functions = functions
import UserForm from './UserFormClass'


const flushPromises = () => new Promise(setImmediate);

describe('Test suite A - user form', () => {

  it('Test 1 - Test initialization of components (and variables)', async () => {
    const { getByTestId, getByText } = render(<UserForm />)

    expect(getByTestId("myStatus")).toBeTruthy()
    expect(getByTestId("myStatus").innerHTML).toEqual("NOT DONE!")
  });

  it('Test 2 - Name input', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    expect(getByTestId("name")).toBeTruthy()
  });
  it('Test 3 - email input', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    expect(getByTestId("email")).toBeTruthy()
  });
  it('Test 4 - phone input', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    expect(getByTestId("phone")).toBeTruthy()
  });
  it('Test 5 - bio input', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    expect(getByTestId("bio")).toBeTruthy()
  });
  it('Test 6 - cv input', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    expect(getByTestId("cv")).toBeTruthy()
  });
  it('Test 7 - submit button', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    expect(getByTestId("myButton")).toBeTruthy()
  });

});

describe('Test suite B - user form input is editable, and our handleInputChange function works', () => {

  it('Test 1 - Name input editable', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    const nameInput = getByTestId("name")

    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    expect(nameInput.value).toEqual("adam");
  });
  it('Test 2 - Email input editable', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    const emailInput = getByTestId("email")

    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    expect(emailInput.value).toEqual("adodgen@sisu.mx");
  });
  it('Test 3 - Phone input editable', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    const phoneInput = getByTestId("phone")

    fireEvent.change(phoneInput, { target: { name: "phone", value: "123456789" } })
    expect(phoneInput.value).toEqual("123456789");
  });
  it('Test 4 - Bio input editable', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    const bioInput = getByTestId("bio")

    fireEvent.change(bioInput, { target: { name: "bio", value: "Hola soy Adam" } })
    expect(bioInput.value).toEqual("Hola soy Adam");

  });
  it('Test 5 - CV input editable', () => {
    const { getByTestId, getByText } = render(<UserForm />)
    const cvInput = getByTestId("cv")
    const cvNameLabel = getByTestId("cvNameLabel")

    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })
    expect(cvNameLabel.innerHTML).toEqual("example-doc-name");
  });

});





describe('Test suite C - user submits form', () => {


  it('Test 1 - Calls submit only (name) error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    //fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)

    expect(getByTestId("nameRequiredError")).toBeTruthy()
    expect(getByTestId("nameRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("emailRequiredError")).toBeNull()
    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
    expect(queryByTestId("cvRequiredError")).toBeNull()
  });

  it('Test 2 - Calls submit only (email) error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    //fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)

    expect(getByTestId("emailRequiredError")).toBeTruthy()
    expect(getByTestId("emailRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("nameRequiredError")).toBeNull()
    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
    expect(queryByTestId("cvRequiredError")).toBeNull()
  });

  it('Test 3 - Calls submit only (phone) error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    //fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)

    expect(getByTestId("phoneRequiredError")).toBeTruthy()
    expect(getByTestId("phoneRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("nameRequiredError")).toBeNull()
    expect(queryByTestId("emailRequiredError")).toBeNull()
    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
    expect(queryByTestId("cvRequiredError")).toBeNull()
  });

  it('Test 4 - Calls submit only (cv) error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    //fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)

    expect(getByTestId("cvRequiredError")).toBeTruthy()
    expect(getByTestId("cvRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("nameRequiredError")).toBeNull()
    expect(queryByTestId("emailRequiredError")).toBeNull()
    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()

  });

  it('Test 5 - Calls submit only (name, email) error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    //fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    //fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)

    expect(getByTestId("nameRequiredError")).toBeTruthy()
    expect(getByTestId("nameRequiredError").innerHTML).toEqual("This field is required")
    expect(getByTestId("emailRequiredError")).toBeTruthy()
    expect(getByTestId("emailRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
    expect(queryByTestId("cvRequiredError")).toBeNull()
  });

  it('Test 6 - Calls submit only (email, cv) error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    //fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    //fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)


    expect(getByTestId("emailRequiredError")).toBeTruthy()
    expect(getByTestId("emailRequiredError").innerHTML).toEqual("This field is required")
    expect(getByTestId("cvRequiredError")).toBeTruthy()
    expect(getByTestId("cvRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("nameRequiredError")).toBeNull()
    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
  });

  it('Test 7 - Calls submit with only phone length error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "123456789" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)


    expect(getByTestId("phoneLengthError")).toBeTruthy()
    expect(getByTestId("phoneLengthError").innerHTML).toEqual("Your phone number must be minimum 10 digits")


    expect(queryByTestId("nameRequiredError")).toBeNull()
    expect(queryByTestId("emailRequiredError")).toBeNull()
    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("cvRequiredError")).toBeNull()

  });

  it('Test 8 - Calls submit with only email format error', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })

    fireEvent.click(myButton)


    expect(getByTestId("emailFormatError")).toBeTruthy()
    expect(getByTestId("emailFormatError").innerHTML).toEqual("Your email must be formatted properly")


    expect(queryByTestId("nameRequiredError")).toBeNull()
    expect(queryByTestId("emailRequiredError")).toBeNull()
    expect(queryByTestId("phoneRequiredError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
    expect(queryByTestId("cvRequiredError")).toBeNull()

  });

  it('Test 9 - Calls submit with 4 errors', async () => {
    const { getByTestId, getByText, queryByTestId } = render(<UserForm />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")

    fireEvent.click(myButton)


    expect(getByTestId("nameRequiredError")).toBeTruthy()
    expect(getByTestId("nameRequiredError").innerHTML).toEqual("This field is required")
    expect(getByTestId("emailRequiredError")).toBeTruthy()
    expect(getByTestId("emailRequiredError").innerHTML).toEqual("This field is required")
    expect(getByTestId("phoneRequiredError")).toBeTruthy()
    expect(getByTestId("phoneRequiredError").innerHTML).toEqual("This field is required")
    expect(getByTestId("cvRequiredError")).toBeTruthy()
    expect(getByTestId("cvRequiredError").innerHTML).toEqual("This field is required")

    expect(queryByTestId("emailFormatError")).toBeNull()
    expect(queryByTestId("phoneLengthError")).toBeNull()
  });

});



describe('Test suite D - Testing the mocked async functions', () => {
  let history = { push: jest.fn(() => { }) }

  it('test 1 - Calls submit form without errors', async () => {

    const { getByTestId, getByText, findByTestId, findByText, queryByTestId } = render(<UserForm history={history} />)

    const nameInput = getByTestId("name")
    const emailInput = getByTestId("email")
    const phoneInput = getByTestId("phone")
    const cvInput = getByTestId("cv")

    const myButton = getByTestId("myButton")


    fireEvent.change(nameInput, { target: { name: "name", value: "adam" } })
    fireEvent.change(emailInput, { target: { name: "email", value: "adodgen@sisu.mx" } })
    fireEvent.change(phoneInput, { target: { name: "phone", value: "1234567890" } })
    fireEvent.change(cvInput, { target: { files: [{ name: "example-doc-name" }] } })


    fireEvent.click(myButton)

    await Promise.resolve()
    var elem1 = getByText("UPLOADED USER!!!")
    expect(elem1).toBeTruthy()

    await Promise.resolve()
    var elem2 = getByText("LOADING TO STORAGE!!!")
    expect(elem2).toBeTruthy()

    await Promise.resolve()
    var elem3 = getByText("GOT URL!!!")
    expect(elem3).toBeTruthy()

    await Promise.resolve()
    var elem4 = getByText("SET URL!!!")
    expect(elem4).toBeTruthy()

    await Promise.resolve()
    var elem5 = getByText("UPLOADED TO TRELLO!!!")
    expect(elem5).toBeTruthy()

    expect(history.push).toHaveBeenCalledWith("/listo")

  })

});

