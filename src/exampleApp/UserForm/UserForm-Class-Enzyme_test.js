import React from 'react';

import { shallow, mount } from 'enzyme';
import { render, act, } from '@testing-library/react'

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, getAllUsers, signInWithEmailAndPassword, uploadCardToTrello } from '../AsyncFunctions'


import firebase from '../../firebase';


import { firestore } from '../firebaseMocks/Firestore'
import { firestoreAddError } from '../firebaseMocks/errors/FirestoreErrors'
firebase.firestore = firestore;
import { storage, fileSnapshot } from '../firebaseMocks/Storage'
firebase.storage = storage;
import { auth } from '../firebaseMocks/Auth'
firebase.auth = auth;
import { functions } from '../firebaseMocks/Functions'
firebase.functions = functions
import UserForm from './UserFormClass'


const flushPromises = () => new Promise(setImmediate);

describe('Test suite A - user form', () => {
  const wrapper = mount(<UserForm />);

  it('Test 1 - Test initialization of state variables', () => {
    expect(wrapper.state().name).toEqual("");
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().phone).toEqual("");
    expect(wrapper.state().bio).toEqual("");
    expect(wrapper.state().cv).toEqual(undefined);
    expect(wrapper.state().cvName).toEqual("");
  });

  it('Test 2 - Name input', () => {
    expect(wrapper.find(".name").exists()).toEqual(true)
  });
  it('Test 3 - email input', () => {
    expect(wrapper.find(".email").exists()).toEqual(true)
  });
  it('Test 4 - phone input', () => {
    expect(wrapper.find(".phone").exists()).toEqual(true)
  });
  it('Test 5 - bio input', () => {
    expect(wrapper.find(".bio").exists()).toEqual(true)
  });
  it('Test 6 - cv input', () => {
    expect(wrapper.find(".cv").exists()).toEqual(true)
  });
  it('Test 7 - submit button', () => {
    expect(wrapper.find(".myButton").exists()).toEqual(true)
  });

});

describe('Test suite B - user form input is editable, and our handleInputChange function works', () => {
  const wrapper = mount(<UserForm />);

  it('Test 1 - Name input editable', () => {
    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    expect(wrapper.state().name).toEqual("adam");
  });
  it('Test 2 - Email input editable', () => {
    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    expect(wrapper.state().email).toEqual("adodgen@sisu.mx");
  });
  it('Test 3 - Phone input editable', () => {
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '1234567890' } });
    expect(wrapper.state().phone).toEqual("1234567890");
  });
  it('Test 4 - Bio input editable', () => {
    wrapper.find('.bio').simulate('change', { target: { name: 'bio', value: 'This is Adams bio' } });
    expect(wrapper.state().bio).toEqual("This is Adams bio");
  });
  it('Test 5 - CV input editable', () => {
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });
    expect(wrapper.state().cv).toEqual({ name: "example-doc-name" });
  });

});





describe('Test suite C - user submits form', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = mount(<UserForm />);
  });

  it('Test 1 - Calls submit only (name) error', async () => {
    expect(wrapper.state().name).toEqual("");

    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '1234567890' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("name")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(false)
  });

  it('Test 2 - Calls submit only (email) error', async () => {
    expect(wrapper.state().email).toEqual("");

    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '1234567890' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("email")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(false)  
  });

  it('Test 3 - Calls submit only (phone) error', async () => {
    expect(wrapper.state().phone).toEqual("");

    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("phone")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(true)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(false)
  });

  it('Test 4 - Calls submit only (cv) error', async () => {
    expect(wrapper.state().cv).toEqual(undefined);

    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '1234567890' } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("cv")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(true)
  });

  it('Test 5 - Calls submit only (name, email) error', async () => {
    expect(wrapper.state().name).toEqual("");
    expect(wrapper.state().email).toEqual("");

    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '1234567890' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(2);
    expect(wrapper.state().errors.includes("name")).toEqual(true);
    expect(wrapper.state().errors.includes("email")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(false)
  });

  it('Test 6 - Calls submit only (email, cv) error', async () => {
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().cv).toEqual(undefined);

    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '1234567890' } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(2);
    expect(wrapper.state().errors.includes("email")).toEqual(true);
    expect(wrapper.state().errors.includes("cv")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(true)
  });

  it('Test 7 - Calls submit with only phone length error', async () => {
    expect(wrapper.state().name).toEqual("");
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().phone).toEqual("");
    expect(wrapper.state().cv).toEqual(undefined);

    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgen@sisu.mx' } });
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '123456789' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("phone")).toEqual(false);
    expect(wrapper.state().errors.includes("phoneLength")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(true)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(false)

  });

  it('Test 8 - Calls submit with only email format error', async () => {
    expect(wrapper.state().name).toEqual("");
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().phone).toEqual("");
    expect(wrapper.state().cv).toEqual(undefined);

    wrapper.find('.name').simulate('change', { target: { name: 'name', value: 'adam' } });
    wrapper.find('.email').simulate('change', { target: { name: 'email', value: 'adodgensis.mc' } });
    wrapper.find('.phone').simulate('change', { target: { name: 'phone', value: '123456789111' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "example-doc-name" }] } });

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(1);
    expect(wrapper.state().errors.includes("email")).toEqual(false);
    expect(wrapper.state().errors.includes("emailFormat")).toEqual(true);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(false)
    expect(wrapper.find('#emailFormatError').exists()).toBe(true)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(false)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(false)

  });

  it('Test 9 - Calls submit with 4 errors', async () => {
    expect(wrapper.state().name).toEqual("");
    expect(wrapper.state().email).toEqual("");
    expect(wrapper.state().phone).toEqual("");
    expect(wrapper.state().cv).toEqual(undefined);

    wrapper.find('.myButton').simulate('click')

    expect(wrapper.state().errors.length).toEqual(4);

    expect(wrapper.find('#nameRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailRequiredError').exists()).toBe(true)
    expect(wrapper.find('#emailFormatError').exists()).toBe(false)
    expect(wrapper.find('#phoneRequiredError').exists()).toBe(true)
    expect(wrapper.find('#phoneLengthError').exists()).toBe(false)
    expect(wrapper.find('#cvRequiredError').exists()).toBe(true)
  });

});



describe('Test suite D - Testing the mocked async functions', () => {
  let wrapper = null
  let history = {push: jest.fn(() => {})}
  beforeEach(() => {
    wrapper = mount(<UserForm history={history}/>);
    firebase.firestore = firestore
  });


  it('test 1 - Calls submit form without errors', async () => {
    expect(wrapper.state().done).toEqual("NOT DONE!");
  
    wrapper.find('.name').simulate('change', { target: { name: "name", value: 'Mikayla Schlenker' } });
    wrapper.find('.email').simulate('change', { target: { name: "email", value: 'mk@mkyl.com' } });
    wrapper.find('.phone').simulate('change', { target: { name: "phone", value: '1234506789' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "exampleDoc" }] } });
  
    expect(wrapper.state().name).toEqual("Mikayla Schlenker");
    expect(wrapper.state().email).toEqual("mk@mkyl.com");
    expect(wrapper.state().phone).toEqual("1234506789");
  
    wrapper.find('.myButton').simulate('click')
  
    expect(wrapper.state().done).toEqual("LOADING!!!");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("LOADING!!!");
  
    await Promise.resolve();
    wrapper.update();
    expect(wrapper.state().done).toEqual("UPLOADED USER!!!");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("UPLOADED USER!!!");
  
    await Promise.resolve();
    wrapper.update();
    expect(wrapper.state().done).toEqual("LOADING TO STORAGE!!!");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("LOADING TO STORAGE!!!");
  
    await Promise.resolve();
    wrapper.update();
    expect(wrapper.state().done).toEqual("GOT URL!!!");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("GOT URL!!!");

    await Promise.resolve();
    wrapper.update();
    expect(wrapper.state().done).toEqual("SET URL!!!");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("SET URL!!!");
  
  })
  
  it('test 2 - Calls submit form with document add error', async () => {
    firebase.firestore = firestoreAddError
    expect(wrapper.state().done).toEqual("NOT DONE!");
  
    wrapper.find('.name').simulate('change', { target: { name: "name", value: 'Mikayla Schlenker' } });
    wrapper.find('.email').simulate('change', { target: { name: "email", value: 'mk@mkyl.com' } });
    wrapper.find('.phone').simulate('change', { target: { name: "phone", value: '1234506789' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "exampleDoc" }] } });
  
    expect(wrapper.state().name).toEqual("Mikayla Schlenker");
    expect(wrapper.state().email).toEqual("mk@mkyl.com");
    expect(wrapper.state().phone).toEqual("1234506789");
  
    wrapper.find('.myButton').simulate('click')
  
    await Promise.resolve();
    wrapper.update();
    expect(wrapper.state().done).toEqual("ERROR ADDING DOCUMENT!!!");
    expect(wrapper.find('.App > div').at(0).props().children).toEqual("ERROR ADDING DOCUMENT!!!");
  })

  it('Calls submit form with page change', async () => {
    expect(wrapper.state().done).toEqual("NOT DONE!");
  
    wrapper.find('.name').simulate('change', { target: { name: "name", value: 'Mikayla Schlenker' } });
    wrapper.find('.email').simulate('change', { target: { name: "email", value: 'mk@mkyl.com' } });
    wrapper.find('.phone').simulate('change', { target: { name: "phone", value: '1234506789' } });
    wrapper.find('.cv').simulate('change', { target: { files: [{ name: "exampleDoc" }] } });
  
    expect(wrapper.state().name).toEqual("Mikayla Schlenker");
    expect(wrapper.state().email).toEqual("mk@mkyl.com");
    expect(wrapper.state().phone).toEqual("1234506789");
  
      wrapper.find('.myButton').simulate('click')

  
    await flushPromises()
    wrapper.update();

    expect(wrapper.state().done).toEqual("UPLOADED TO TRELLO!!!")
  })
});

