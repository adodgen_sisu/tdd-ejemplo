import React, { Component } from 'react';

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, uploadTrelloCard, } from '../AsyncFunctions'

import firebase from '../../firebase';

class UserForm extends Component {

    INITIAL_STATE = {
        name: "",
        email: "",
        phone: "",
        bio: "",
        cv: undefined,
        cvName: "",
        errors: [],
        done: "NOT DONE!"
    };

    constructor(props) {
        super(props);
        this.state = { ...this.INITIAL_STATE };
    }

    componentDidMount() {

    }


    handleFormInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value, errors: [] });
    }
    handleFormInputCV = (event) => {
        this.setState({ cv: event.target.files[0] || undefined, errors: [], cvName: event.target.files[0].name });
    }

    checkForErrors = () => {
        let errors = []
        const required = ["name", "email", "phone", "cv"]
        required.forEach(field => {
            if (this.state[field] === "" || this.state[field] === undefined)
                errors.push(field)
        })

        if (!errors.includes("phone") && this.state.phone.length < 10)
            errors.push("phoneLength")

        let re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9_.-]+\.[a-zA-Z_0-9-]{2,}$/
        if (!errors.includes("email") && !re.test(this.state.email))
            errors.push("emailFormat")

        this.setState({ errors: errors })

        return errors.length > 0
    }


    submitForm = async () => {
        this.setState({ done: "LOADING!!!" })
        if (!this.checkForErrors()) {
            try {
                let doc = await uploadUser({ name: this.state.name, email: this.state.email, phone: this.state.phone, bio: this.state.bio })
                this.setState({ done: "UPLOADED USER!!!" })
                let file = await uploadCVToStorage(this.state.cv, Date.now())
                this.setState({ done: "LOADING TO STORAGE!!!" })
                let url = await getFileURL(file)
                this.setState({ done: "GOT URL!!!" })
                await setUserCV(doc.id, url)
                this.setState({ done: "SET URL!!!" })
                let response = await uploadTrelloCard(this.state.name, this.state.email, this.state.phone, this.state.bio)
                this.setState({ done: "UPLOADED TO TRELLO!!!" })

                this.props.history.push('/listo')
            }
            catch (error) {
                console.log(JSON.stringify(error), " - error")
                this.setState({ done: "ERROR ADDING DOCUMENT!!!" })
            }
            
        }
    }/**/


    render() {
        return (
            <div className="App">

                <div className="myStatus" data-testid="myStatus">{this.state.done}</div>

                <input className="name" name="name" data-testid="name"
                    type="text"
                    placeholder="Nombre completo *"
                    value={this.state.name}
                    onChange={this.handleFormInputChange}
                />
                <div >{this.state.errors.includes("name") && <label style={{ color: 'red' }} id="nameRequiredError" data-testid="nameRequiredError">This field is required</label>}</div>

                <input className="email" name="email" data-testid="email"
                    type="text"
                    placeholder="email"
                    value={this.state.email}
                    onChange={this.handleFormInputChange}
                />
                <div >{this.state.errors.includes("email") && <label style={{ color: 'red' }} id="emailRequiredError" data-testid="emailRequiredError">This field is required</label>}</div>
                <div >{!this.state.errors.includes("email") && this.state.errors.includes("emailFormat") && <label style={{ color: 'red' }} id="emailFormatError" data-testid="emailFormatError">Your email must be formatted properly</label>}</div>

                <input className="phone" name="phone" data-testid="phone"
                    type="text"
                    placeholder="phone"
                    value={this.state.phone}
                    onChange={this.handleFormInputChange}
                />
                <div >{this.state.errors.includes("phone") && <label style={{ color: 'red' }} id="phoneRequiredError" data-testid="phoneRequiredError">This field is required</label>}</div>
                <div >{!this.state.errors.includes("phone") && this.state.errors.includes("phoneLength") && <label style={{ color: 'red' }} id="phoneLengthError" data-testid="phoneLengthError">Your phone number must be minimum 10 digits</label>}</div>

                <input className="bio" name="bio"  data-testid="bio"
                    type="text"
                    placeholder="bio"
                    value={this.state.bio}
                    onChange={this.handleFormInputChange}
                />
                <div />
                <input className="cv" name="cv" data-testid="cv"
                    type="file"
                    placeholder="uploadcv"
                    value=""
                    onChange={this.handleFormInputCV}
                />
                <label data-testid="cvNameLabel">{this.state.cvName}</label>
                <div >{this.state.errors.includes("cv") && <label style={{ color: 'red' }} id="cvRequiredError" data-testid="cvRequiredError">This field is required</label>}</div>

                {<button type="submit" style={{ width: 200, height: 22, backgroundColor: 'gray' }} className="myButton" data-testid="myButton" onClick={this.submitForm}>Submit to Firestore</button>}


            </div>
        )

    }
}

export default UserForm;