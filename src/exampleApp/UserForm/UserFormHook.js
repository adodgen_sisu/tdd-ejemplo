import React, { useState, useEffect, useGlobal } from 'react';

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, uploadTrelloCard, } from '../AsyncFunctions'

import firebase from '../../firebase';

const UserFormHook = (props) => {


    const [state, setState] = useState({
        name: "",
        email: "",
        phone: "",
        bio: "",
        cv: undefined,
        cvName: "",
        errors: [],
        done: "NOT DONE!"
    })


    useEffect(() => {

    }, [])

    useEffect(() => {

    }, [state])

    const handleFormInputChange = (event) => {
        setState({...state,  [event.target.name]: event.target.value, errors: [] });
    }
    const handleFormInputCV = (event) => {
        setState({...state,  cv: event.target.files[0] || undefined, errors: [], cvName: event.target.files[0].name });
    }

    const checkForErrors = () => {
        let errors = []
        const required = ["name", "email", "phone", "cv"]
        required.forEach(field => {
            if (state[field] === "" || state[field] === undefined)
                errors.push(field)
        })

        if (!errors.includes("phone") && state.phone.length < 10)
            errors.push("phoneLength")

        let re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9_.-]+\.[a-zA-Z_0-9-]{2,}$/
        if (!errors.includes("email") && !re.test(state.email))
            errors.push("emailFormat")

        setState({...state,  errors: errors })

        return errors.length > 0
    }


    const submitForm = async () => {
        setState({...state,  done: "LOADING!!!" })
        if (!checkForErrors()) {
            try {
                let doc = await uploadUser({ name: state.name, email: state.email, phone: state.phone, bio: state.bio })
                setState({...state,  done: "UPLOADED USER!!!" })
                let file = await uploadCVToStorage(state.cv, Date.now())
                setState({...state,  done: "LOADING TO STORAGE!!!" })
                let url = await getFileURL(file)
                setState({...state,  done: "GOT URL!!!" })
                await setUserCV(doc.id, url)
                setState({...state,  done: "SET URL!!!" })
                let response = await uploadTrelloCard(state.name, state.email, state.phone, state.bio)
                setState({...state,  done: "UPLOADED TO TRELLO!!!" })

                props.history.push('/listo')
            }
            catch (error) {
                console.log(JSON.stringify(error), " - error")
                setState({...state,  done: "ERROR ADDING DOCUMENT!!!" })
            }
            
        }
    }/**/


        return (
            <div className="App">

                <div className="myStatus" data-testid="myStatus">{state.done}</div>

                <input className="name" name="name" data-testid="name"
                    type="text"
                    placeholder="Nombre completo *"
                    value={state.name}
                    onChange={handleFormInputChange}
                />
                <div >{state.errors.includes("name") && <label style={{ color: 'red' }} id="nameRequiredError" data-testid="nameRequiredError">This field is required</label>}</div>

                <input className="email" name="email" data-testid="email"
                    type="text"
                    placeholder="email"
                    value={state.email}
                    onChange={handleFormInputChange}
                />
                <div >{state.errors.includes("email") && <label style={{ color: 'red' }} id="emailRequiredError" data-testid="emailRequiredError">This field is required</label>}</div>
                <div >{!state.errors.includes("email") && state.errors.includes("emailFormat") && <label style={{ color: 'red' }} id="emailFormatError" data-testid="emailFormatError">Your email must be formatted properly</label>}</div>

                <input className="phone" name="phone" data-testid="phone"
                    type="text"
                    placeholder="phone"
                    value={state.phone}
                    onChange={handleFormInputChange}
                />
                <div >{state.errors.includes("phone") && <label style={{ color: 'red' }} id="phoneRequiredError" data-testid="phoneRequiredError">This field is required</label>}</div>
                <div >{!state.errors.includes("phone") && state.errors.includes("phoneLength") && <label style={{ color: 'red' }} id="phoneLengthError" data-testid="phoneLengthError">Your phone number must be minimum 10 digits</label>}</div>

                <input className="bio" name="bio"  data-testid="bio"
                    type="text"
                    placeholder="bio"
                    value={state.bio}
                    onChange={handleFormInputChange}
                />
                <div />
                <input className="cv" name="cv" data-testid="cv"
                    type="file"
                    placeholder="uploadcv"
                    value=""
                    onChange={handleFormInputCV}
                />
                <label data-testid="cvNameLabel">{state.cvName}</label>
                <div >{state.errors.includes("cv") && <label style={{ color: 'red' }} id="cvRequiredError" data-testid="cvRequiredError">This field is required</label>}</div>

                {<button type="submit" style={{ width: 200, height: 22, backgroundColor: 'gray' }} className="myButton" data-testid="myButton" onClick={submitForm}>Submit to Firestore</button>}


            </div>
        )

}

export default UserFormHook;