import React, { Component } from 'react';
import firebase from '../../firebase'

import { getAllUsers } from '../AsyncFunctions'


class UsersTable extends Component {


    INITIAL_STATE = {
        users: [],
        loading: true, mm: 55
    };

    constructor(props) {
        super(props);
        this.state = { ...this.INITIAL_STATE };
    }

    componentDidMount() {
        this.getAllUsers()
    }

    getAllUsers = async () => {
        let users = await getAllUsers()

        this.setState({ users: users.docs, loading: false })
    }


    logout = async () => {
        await firebase.auth().signOut()
        this.props.history.push('/login')
    }

    render() {
        return (
            <div className="App">
                <br /><br />

                All Users


            <table style={{ width: "100%" }} id="usersTable" data-testid="usersTable">
                    <tbody id="usersTableBody" data-testid="usersTableBody">
                        <tr>
                            <th>Order</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Bio</th>
                        </tr>
                        {this.state.users.map((user, index) => {
                            return <tr>
                                <th>User #{index}</th>
                                <th>{user.data().name}</th>
                                <th>{user.data().email}</th>
                                <th>{user.data().phone}</th>
                                <th>{user.data().bio}</th>
                            </tr>
                        })}
                    </tbody>
                </table>

                <button style={{ width: 200, height: 22, backgroundColor: 'gray' }} id="logoutButton" data-testid="logoutButton" onClick={this.logout}>Logout</button>
                <br /><br /><br />
                {this.state.loading && <label style={{ fontSize: 28 }} id="loadingLabel" data-testid="loadingLabel">Loading!</label>}
                {this.state.users.length === 0 && this.state.loading === false && <label style={{ fontSize: 28 }} id="noUsersLabel" data-testid="noUsersLabel">There are no applications!</label>}


            </div>
        )

    }
}


export default UsersTable;