import React from 'react';
import { render, fireEvent, act, wait, waitFor, waitForElement, toBeTruthy } from '@testing-library/react'

import firebase from '../../firebase';

import UsersTable from './UsersTableClass'
import { firestore, firestoreEmptyColl } from '../firebaseMocks/Firestore'
firebase.firestore = firestore
import { auth } from '../firebaseMocks/Auth'
firebase.auth = auth



describe('Test suite A - User input ', () => {

  it('Test 1 - Test initialization that displays loading text to user', () => {
    const { getByTestId, getByText } = render(<UsersTable />)
    expect(getByText("Loading!")).toBeTruthy()
  });

  it('Test 2 - Test users table is in the form', () => {
    const { getByTestId, getByText } = render(<UsersTable />)
    expect(getByTestId("usersTable")).toBeVisible()
  });

  it('Test 3 - Test logout button is in the form', () => {
    const { getByTestId, getByText } = render(<UsersTable />)
    expect(getByText("Logout")).toBeVisible()
  });

});



describe('Test suite B - Get users function', () => {
  let history = { push: jest.fn(() => { }) }

  it('Test 1 - Test get all users and display in table', async () => {

    const { findByTestId, findByText, getByText, getByTestId } = await render(<UsersTable />)
    expect(firestore().collection).toHaveBeenCalledWith("uploadedUsers");

    //var user = await waitForElement(() => getByText("User #0")) //otro alternativa que hace lo mismo
    let user = await findByText("User #0")
    expect(user).toBeVisible();

  })

  it('Test 2 - Test get no users and display message', async () => {
    firebase.firestore = firestoreEmptyColl

    const { findByTestId, findByText, getByText, getByTestId } = await render(<UsersTable />)
    expect(firestore().collection).toHaveBeenCalledWith("uploadedUsers");

    let noApplicationsLabel = await waitForElement(() => getByText("There are no applications!"))
    expect(noApplicationsLabel).toBeTruthy()
    expect(noApplicationsLabel).toBeVisible();
  })

  it('Test 3 - Test get no users and display message USING act', async () => {
    firebase.firestore = firestoreEmptyColl
    let findByTestId = null, findByText = null, getByText = null, getByTestId = null
    await act(async () => {
      ({ findByTestId, findByText, getByText, getByTestId } = render(<UsersTable />))
    })

    expect(getByText("There are no applications!")).toBeTruthy()
    expect(getByText("There are no applications!")).toBeVisible();
  })

});


describe('Test suite C - Logout functionality', () => {
  let history = {push: jest.fn(() => {})}
  
  it('Test 1 - Test logout and push to next screen', async () => {
    let { findByTestId, findByText, getByText, getByTestId } = render(<UsersTable history={history}/>)
    const logoutButton = getByTestId("logoutButton")

    await act(async() => {
      fireEvent.click(logoutButton)
    })

    expect(firebase.auth().signOut).toHaveBeenCalled()
    expect(history.push).toHaveBeenCalled()
  })
});