import React, { useState, useEffect, useGlobal } from 'react';
import firebase from '../../firebase'

import { getAllUsers, signOut } from '../AsyncFunctions'


const UsersTableHook = (props) => {


    const [state, setState] = useState({ users: [], loading: true })

    useEffect(() => {
        getUsers()
    }, [])

    useEffect(() => {

    }, [state])

    const getUsers = async () => {
        let allUsers = await getAllUsers()
        setState({ ...state, users: allUsers.docs, loading: false })
    }


    const logout = async () => {
        await signOut()
        props.history.push('/login')
    }

    return (
        <div className="App">
            <br /><br />

            All Users


            <table style={{ width: "100%" }} id="usersTable" data-testid="usersTable">
                <tbody id="usersTableBody" data-testid="usersTableBody">
                    <tr>
                        <th>Order</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Bio</th>
                    </tr>
                    {state.users.map((user, index) => {
                        return <tr>
                            <th>User #{index}</th>
                            <th>{user.data().name}</th>
                            <th>{user.data().email}</th>
                            <th>{user.data().phone}</th>
                            <th>{user.data().bio}</th>
                        </tr>
                    })}
                </tbody>
            </table>

            <button style={{ width: 200, height: 22, backgroundColor: 'gray' }} id="logoutButton" data-testid="logoutButton" onClick={logout}>Logout</button>
            <br /><br /><br />
            {state.loading && <label style={{ fontSize: 28 }} id="loadingLabel" data-testid="loadingLabel">Loading!</label>}
            {state.users.length === 0 && state.loading === false && <label style={{ fontSize: 28 }} id="noUsersLabel" data-testid="noUsersLabel">There are no applications!</label>}


        </div>
    )

}


export default UsersTableHook;