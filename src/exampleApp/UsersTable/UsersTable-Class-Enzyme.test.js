import React from 'react';
import { render } from '@testing-library/react';

import { shallow, mount } from 'enzyme';

import { uploadUser, uploadCVToStorage, getFileURL, setUserCV, getAllUsers, signInWithEmailAndPassword, uploadCardToTrello } from '../AsyncFunctions'


import firebase from '../../firebase';


import UsersTable from './UsersTableClass'
import {firestore, firestoreEmptyColl} from '../firebaseMocks/Firestore'
firebase.firestore = firestore
import {auth} from '../firebaseMocks/Auth'
firebase.auth = auth

const flushPromises = () => new Promise(setImmediate);

describe('Test suite A - User input', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = mount(<UsersTable />);
  });

  it('Test 1 - Test initialization of state variables', async () => {
    wrapper = mount(<UsersTable />);
    expect(wrapper.state().users).toEqual([]);
    expect(wrapper.state().loading).toEqual(true);
  });

  it('Test 2 - Test users table component exists', () => {
    expect(wrapper.find("#usersTable").exists()).toEqual(true)
  });

  it('Test 3 - Test logout button exists', () => {
    expect(wrapper.find("#logoutButton").exists()).toEqual(true)
  });

  it('Test 4 - Loading message shown at start', () => {
    expect(wrapper.find("#loadingLabel").exists()).toEqual(true)
  });
  
});



describe('Test suite B - Get users function', () => {
  let history = {push: jest.fn(() => {})}
  let wrapper = null

  it('Test 1 - Test get all users and display in table', async () => {
    wrapper = mount(<UsersTable history={history}/>);
    expect(wrapper.state().loading).toEqual(true)
    expect(wrapper.state().users).toEqual([]);

    await flushPromises()

    expect(firestore().collection).toHaveBeenCalledWith("uploadedUsers");
    expect(wrapper.state().loading).toEqual(false)
    expect(wrapper.state().users.length).not.toEqual(0)

    wrapper.update()
    expect(wrapper.find('#loadingLabel').exists()).toEqual(false)
    expect(wrapper.find('#usersTableBody').at(0).props().children[1].length).toEqual(3);
  })

  it('Test 2 - Test get no users and display message', async () => {
    firebase.firestore = firestoreEmptyColl

    wrapper = mount(<UsersTable history={history}/>);
    expect(wrapper.state().loading).toEqual(true)
    expect(wrapper.state().users).toEqual([]);

    await flushPromises()

    expect(wrapper.state().loading).toEqual(false)
    expect(wrapper.state().users.length).toEqual(0);

    wrapper.update()
    expect(wrapper.find('#loadingLabel').exists()).toEqual(false)
    expect(wrapper.find('#noUsersLabel').exists()).toEqual(true)
    expect(wrapper.find('#usersTableBody').at(0).props().children[1].length).toEqual(0);
  })
});


describe('Test suite C - Logout functionality', () => {
  let history = {push: jest.fn(() => {})}
  
  it('Test 1 - Test logout and push to next screen', async () => {
    let wrapper = mount(<UsersTable history={history}/>);

    wrapper.find('#logoutButton').simulate('click')
    expect(firebase.auth().signOut).toHaveBeenCalled()

    await flushPromises()

    expect(history.push).toHaveBeenCalledWith('/login')
  });
});