const firestore = () => {
  return { collection }
}

let collection = jest.fn(() => {
  return { 
    get: jest.fn(() => Promise.resolve(collResult)),
    doc,
    add: add
  }
})

const doc = jest.fn(() => {
  return{
    get: jest.fn(() => Promise.resolve(docResult)),
    set
  }
})

const set = jest.fn(() => Promise.resolve())
let add = jest.fn(() => Promise.resolve(docRef))

const docResult = {
  id: "12345678901234567890",
  data: () => docData
}
const docResult2 = {
  id: "12345678901234567890",
  data: () => docData2
}
const docResult3 = {
  id: "12345678901234567890",
  data: () => docData3
}

const docData = {
  name: "MOCK NAME",
  email: "MOCK EMAIL",
  phone: "MOCK PHONE",
  bio: "MOCK BIO",
  cv: "MOCK URL"
}
const docData2 = {
  name: "MOCK NAME2",
  email: "MOCK EMAIL2",
  phone: "MOCK PHONE2",
  bio: "MOCK BIO2",
  cv: "MOCK URL2"
}
const docData3 = {
  name: "MOCK NAME3",
  email: "MOCK EMAIL3",
  phone: "MOCK PHONE3",
  bio: "MOCK BIO3",
  cv: "MOCK URL3"
}

const docRef = {
  id: "12345678901234567890"
}

const collResult = {
  docs: [docResult,docResult2,docResult3],
  empty: false
}

const collResultEmpty = {
  docs: [],
  empty: true
}

const firestoreEmptyColl = () => {
  return { collection: collectionEmpty }
}

let collectionEmpty = jest.fn(() => {
  return { 
    get: jest.fn(() => Promise.resolve(collResultEmpty)),

  }
})

export { firestore, firestoreEmptyColl }