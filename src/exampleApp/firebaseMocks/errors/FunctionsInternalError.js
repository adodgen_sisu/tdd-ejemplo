const functionsInternalError = () => {
  return { httpsCallable }
}

const httpsCallable = jest.fn(() => {
  
  return jest.fn(() => Promise.reject({message: "Internal error", code: 500}))

})


export { functionsInternalError }