const firestore = () => {
  return { collection }
}

let collection = jest.fn(() => {
  return { 
    get: jest.fn(() => Promise.resolve(collResult)),
    doc,
    add: addError1
  }
})

const doc = jest.fn(() => {
  return{
    get: jest.fn(() => Promise.resolve(docResult)),
    set
  }
})

const set = jest.fn(() => Promise.resolve())
let addError1 = jest.fn(() => Promise.reject({message: "ERROR ADDING DOC", code: 771}))
let addError2 = jest.fn(() => Promise.reject({message: "ERROR ADDING DOC2", code: 772}))

export { firestore as firestoreAddError }