
const rejectNotFound = jest.fn(() => Promise.reject({code: 'auth/user-not-found', message: "There is no existing user record corresponding to the provided identifier."}))
const rejectWrongPassword = jest.fn(() => Promise.reject({code: 'auth/wrong-password', message: "The provided value for the password user property is invalid. It must be a string with at least six characters"}))

const authNotFoundError = () => {
  return { 
    signInWithEmailAndPassword: rejectNotFound
   };
   
};

const authInvalidPassError = () => {
  return { 
    signInWithEmailAndPassword: rejectWrongPassword
   };
   
};

export { authNotFoundError, authInvalidPassError };