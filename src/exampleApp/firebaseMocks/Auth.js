const returnedUser = {user: {uid: "28dj4mao60148cmtb25k", email: "MOCK SIGNIN MAIL"}}

const signInWithEmailAndPassword = jest.fn(() => Promise.resolve(returnedUser))
const signOut = jest.fn(() => Promise.resolve())

const auth = () => {
  return { 
    signInWithEmailAndPassword,
    signOut,
   };
   
};

export { auth };