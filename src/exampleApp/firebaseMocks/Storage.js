const downloadURL = "https://exampleDownloadURL/1unbcuain324nfb/f23n098n2f3";

const getDownloadURL = jest.fn(() => Promise.resolve(downloadURL));
const fileSnapshot = {
  ref: {getDownloadURL: getDownloadURL }
};


const storage = () => {
  return { ref };
};
const ref = () => {
  return { child };
};
const child = jest.fn(() => {
  return { put };
});

const put = jest.fn(() => Promise.resolve(fileSnapshot))

export { storage, fileSnapshot };