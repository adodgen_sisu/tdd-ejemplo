import React, { Component } from 'react';

class RangeCounterClass extends Component {
  constructor(props) {
    super(props);
    const { min } = props;
    this.state = {
      counter: min,
      hasEdited: false,
    };
  }

  componentDidUpdate() {

  }

  incrementCounter = () => {
    this.setState({counter: this.state.counter + 1, hasEdited: true})
  } 

  render() {
    const { max, min } = this.props;
    return (
      <div className="RangeCounter">

<div data-testid="counter-value">{this.state.counter}</div>

        <span className="RangeCounter__title">Class RangeCounter</span>
        <div className="RangeCounter__controls">

          <button className="myButton"
            disabled={this.state.counter >= max}
            onClick={this.incrementCounter}>
              +
          </button>

        </div>

{(this.state.counter >= max && this.state.hasEdited) &&
  <span className="RangeCounterAlert">Range limit reached.</span>
}
      </div>
    );
  }
}

export default RangeCounterClass