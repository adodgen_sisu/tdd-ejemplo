import React, { useState, useEffect, useGlobal } from 'react';

const RangeCounter = props => {
    const { max, min } = props;
    const [counter, setCounter] = useState(min)
    const [hasEdited, setHasEdited] = useState(false)

    useEffect(() => {
        if(counter !== min && !hasEdited)
          setHasEdited(true)
    }, [counter, hasEdited])


    
    return (
      <div className="RangeCounter">
        <span className="RangeCounter__title">Functional RangeCounter</span>
        <div  className="RangeCounter__controls">

          <span data-testid="counter-value" id="counter-value">{counter}</span>
        <button className="myButton"
        disabled={counter >= max}
        onClick={() => setCounter(counter + 1)}>

        </button>

        </div>
      {(counter >= max && hasEdited) && 
      <span className="RangeCounterAlert">Range limit reached.</span>}

      </div>
    );
  };

  export default RangeCounter