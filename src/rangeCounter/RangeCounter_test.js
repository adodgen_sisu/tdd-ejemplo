import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Enzyme, { mount } from 'enzyme';

import RangeCounterClass from './RangeCounterClass'
import RangeCounter from './RangeCounter'


//Enzyme
describe("RangeCounter ENZYME", () => {
  let component;

  it("updates counter value correctly CLASS", () => {
    component = mount(<RangeCounterClass min={0} />)
    component.instance().incrementCounter()
    expect(component.state().counter).toEqual(1)
  });

  it("shows range reached alert when reached limit by clicking control buttons CLASS",
    () => {
      component = mount(<RangeCounterClass min={0} max={1} />)
      component.instance().incrementCounter()
      component.update()
      const alert = component.find('.RangeCounterAlert')
      expect(alert.text()).toEqual('Range limit reached.')
    });

  it("updates the counter value HOOKS", () => {
    component = mount(<RangeCounter min={0} />)
    component.find('.myButton').simulate('click')
    const num = component.find('#counter-value')
    expect(num.text()).toEqual('1')

  });

  it("shows range reached alert when reached limit by clicking control buttons HOOKS",
    () => {
      component = mount(<RangeCounter min={0} max={1} />)
      component.find('.myButton').simulate('click')
      const alert = component.find('.RangeCounterAlert')
      expect(alert.text()).toEqual('Range limit reached.')

    }
  );

});



//React Testing Library
describe("RangeCounter RTL", () => {

  it("updates the counter value CLASS", () => {
    const { getByTestId, getByText } = render(<RangeCounterClass min ={0} />)
    const incrementButton = getByText("+")
    fireEvent.click(incrementButton)
    expect(getByTestId("counter-value").innerHTML).toEqual("1")

  });

  it("shows range reached alert when reached limit by clicking control buttons CLASS",
    () => {
      const { getByText } = render(<RangeCounterClass min ={0} max={1} />)
      const incrementButton = getByText("+")
      fireEvent.click(incrementButton)
      expect(getByText("Range limit reached.")).toBeVisible()

    })

  it("updates the counter value HOOKS", () => {
    const { getByTestId, getByText } = render(<RangeCounterClass min ={0} />)
    const incrementButton = getByText("+")
    fireEvent.click(incrementButton)
    expect(getByTestId("counter-value").innerHTML).toEqual("1")

  });

  it("shows range reached alert when reached limit by clicking control buttons HOOKS",
    () => {
      const { getByText } = render(<RangeCounterClass min ={0} max={1} />)
      const incrementButton = getByText("+")
      fireEvent.click(incrementButton)
      expect(getByText("Range limit reached.")).toBeVisible()
    }
  );
});