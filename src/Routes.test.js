import React, { Component } from 'react';
import Enzyme, { mount } from 'enzyme';

import firebase from './firebase'

import Routes from './Routes';


describe('Test suite - Testing the auth state routes', () => {
  let wrapper = null

  beforeEach(() => {
    wrapper = mount(<Routes  />);
  })

  it('Test 1 - Done loading with routes available without signed out user', async () => {
    wrapper.setState({loggedIn: false, isLoading: false})
    expect(wrapper.state().loggedIn).toEqual(false)
    expect(wrapper.state().isLoading).toEqual(false)

    expect(wrapper.find('#emptyDiv').exists()).toEqual(false)
    expect(wrapper.find('#UserFormRoute').exists()).toEqual(true)
    expect(wrapper.find('#LoginRoute').exists()).toEqual(true)
    expect(wrapper.find('#ListoRoute').exists()).toEqual(true)
    expect(wrapper.find('#UsersTableRoute').exists()).toEqual(false)

    expect(wrapper.find('#UsersTableToLoginRoute').exists()).toEqual(true)

  })

  it('Test 2 - Done loading with routes available with signedin user', async () => {
    wrapper.setState({loggedIn: true, isLoading: false})
    expect(wrapper.state().loggedIn).toEqual(true)
    expect(wrapper.state().isLoading).toEqual(false)

    expect(wrapper.find('#emptyDiv').exists()).toEqual(false)
    expect(wrapper.find('#UserFormRoute').exists()).toEqual(true)
    expect(wrapper.find('#LoginRoute').exists()).toEqual(true)
    expect(wrapper.find('#ListoRoute').exists()).toEqual(true)

    expect(wrapper.find('#UsersTableRoute').exists()).toEqual(true)

  })


});

