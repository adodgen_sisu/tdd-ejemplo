import React, { Component } from 'react';

import firebase from './firebase'

import UserForm from './exampleApp/UserForm/UserFormClass';
import UserTable from './exampleApp/UsersTable/UsersTableClass';
import Login from './exampleApp/Login/LoginClass';
import LoginHook from './exampleApp/Login/LoginHook';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

export default class Routes extends Component {

    constructor(props) {
        super(props)
        this.state = { loggedIn: false, isLoading: true };
    }
    componentDidMount() {
        this.checkAuthState()
    }
    checkAuthState = async () => {

        try {
            firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    this.setState({ loggedIn: true, isLoading: false })
                } else {

                    this.setState({ loggedIn: false, isLoading: false })
                }
            })

        } catch (error) {
            this.setState({ isLoading: false })
            console.log("error", error)
        }

    }
    render() {
        if (this.state.isLoading)
            return <div id="emptyDiv"></div>
        else {
            return (
                    <BrowserRouter id="BrowserRouter">
                        <Switch id="BrowserRouterSwitch">

                            <React.Fragment>
                                <Route id="UserFormRoute" exact path="/user-form" component={UserForm} />
                                <Route id="LoginRoute" exact path="/login" component={Login} />
                                <Route id="ListoRoute" exact path="/listo" component={() => <div>LISTO!</div>} />

                                <Route id="LoginHookRoute" exact path="/login-hook" component={LoginHook} />

                                {this.state.loggedIn && <Route id="UsersTableRoute" exact path="/users-table" component={UserTable} />}
                                {!this.state.loggedIn && <Route id="UsersTableToLoginRoute" exact path="/users-table" component={Login} />}
                            </React.Fragment>

                        </Switch>

                    </BrowserRouter>
            )
        }

    }
}