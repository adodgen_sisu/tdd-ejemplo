import * as firebase from 'firebase';

// Initialize Firebase
const config = {
    apiKey: "AIzaSyAHigs1R1gwVbWHqVazn0WeWfArKkRaEAo",
    authDomain: "tdd-proyecto-de-ejemplo.firebaseapp.com",
    databaseURL: "https://tdd-proyecto-de-ejemplo.firebaseio.com",
    projectId: "tdd-proyecto-de-ejemplo",
    storageBucket: "tdd-proyecto-de-ejemplo.appspot.com",
    messagingSenderId: "617280397864",
    appId: "1:617280397864:web:d9585fd928644d6792cb1d"
};

const fire = firebase.initializeApp(config);

export default fire